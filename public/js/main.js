$(document).ready(function(){
  $('.select2').select2({
      placeholder: "Seleccionar",
      allowClear: true,
    });



// FACTURAS
  $fclientes = $('#facturas-cliente');
  $fcarros = $('#facturas-carros');
  $foferta = $('#facturas-oferta');
  $foferta.hide();
  
  
  $fclientes.select2({
    placeholder: "Seleccionar",
    allowClear: true,
    tags:true,
    createTag: function (tag) {
      return {
        id: tag.term,
        text: tag.term,
            // add indicator:
            isNew : true
          };
        }
      }).on("select2:select", function(e) {
        if(e.params.data.isNew){
        // append the new option element prenamently:
        $(this).find('[value="'+e.params.data.id+'"]').replaceWith('<option selected value="'+e.params.data.id+'">'+e.params.data.text+'</option>');
        window.location.replace('../facturas/new/' + e.params.data.id);
      }
    });

  $fclientes.on('change',function(){
    let carros = $(this).find(':selected').data('carros');
    let options = '';

    $fcarros.html('');

    if (typeof carros !== 'undefined'){
      carros.forEach( function(elem,indx){
        options += '<option value="'+ elem.id +'">'+ elem.modelo + ' - Placa: ' + elem.placa + '</option>'
      });
    }

    $fcarros.html(options);
    
    // Promocion
    let promote = $(this).find(':selected').data('offer');
    if(promote == 1){
      $foferta.fadeIn();
    }else{
      $foferta.hide();
    }
  });
});