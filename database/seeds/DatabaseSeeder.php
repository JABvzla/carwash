<?php

use Illuminate\Database\Seeder;
use App\Models\Users;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new Users();
        $user->name =  "admin";
        $user->email = "admin@email.com";
        $user->password =  Hash::make("abc789");
        $user->save();
        echo "User Admin has been created";
    }
}
