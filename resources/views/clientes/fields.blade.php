@php
  if(!isset($cedula)){
    $cedula = null;
  }
@endphp

<!-- Cedula Field -->
<div class="form-group col-sm-5 col-sm-offset-3">
    {!! Form::label('cedula', 'Cedula:') !!}
    {!! Form::number('cedula', $cedula, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Field -->
<div class="form-group col-sm-5 col-sm-offset-3">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Apellido Field -->
<div class="form-group col-sm-5 col-sm-offset-3">
    {!! Form::label('apellido', 'Apellido:') !!}
    {!! Form::text('apellido', null, ['class' => 'form-control']) !!}
</div>


@if( !isset($cedula))
  <!-- Submit Field -->
  <div class="form-group col-sm-3 col-sm-offset-3">
      {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
      <a href="{!! route('clientes.index') !!}" class="btn btn-default">Cancelar</a>
  </div>
@endif