<li class="{{ Request::is('clientes*') ? 'active' : '' }}">
    <a href="{!! route('clientes.index') !!}"><i class="fa fa-user"></i><span>Clientes</span></a>
</li>

<li class="{{ Request::is('carros*') ? 'active' : '' }}">
    <a href="{!! route('carros.index') !!}"><i class="fa fa-car"></i><span>Carros</span></a>
</li>

<li class="{{ Request::is('servicios*') ? 'active' : '' }}">
    <a href="{!! route('servicios.index') !!}"><i class="fa fa-edit"></i><span>Servicios</span></a>
</li>

<li class="{{ Request::is('facturas*') ? 'active' : '' }}">
    <a href="{!! route('facturas.index') !!}"><i class="fa fa-list"></i><span>Facturas</span></a>
</li>

<li>
    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
      <i class="fa fa-sign-out"></i><span>Salir</span>
    </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
</li>
