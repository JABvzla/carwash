
<input type="text" id="date-created" class="daterange input-form" value="01/01/2015 - 01/31/2015" />

<table class="table table-responsive" id="facturas-table">
    <thead>
        <th>Fecha</th>
        <th>Servicio</th>
        <th>Precio</th>
        <th>Cedula</th>
        <th>Nombre</th>
        <th>Carro</th>
        <th>Placa</th>
        <th>Action</th>
    </thead>
    <tbody>
    @foreach($facturas as $facturas)
        <tr>
            <td>{!! $facturas->created !!}</td>
            <td>{!! $facturas->servicio !!}</td>
            <td>{!! $facturas->precio !!}</td>
            <td>{!! $facturas->carro->cliente->cedula !!}</td>
            <td>{!! $facturas->carro->cliente->nombre !!}</td>
            <td>{!! $facturas->carro->modelo !!}</td>
            <td>{!! $facturas->carro->placa !!}</td>
            <td>
                {!! Form::open(['route' => ['facturas.destroy', $facturas->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('facturas.show', [$facturas->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    {{-- <a href="{!! route('facturas.edit', [$facturas->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a> --}}
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Está seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
