<div class="offer" id="facturas-oferta">
  Esta factura se cobrara a mitad de precio gracias a la promocion
</div>
<br><br><br>
<div class="form-group col-sm-6 col-sm-offset-3">
  {!! Form::label('cliente', 'Cliente:') !!}
  <select class="form-control col-sm-6 select2" id="facturas-cliente" tabindex="1" autofocus required>
    <option></option>
    @foreach($clientes as $cliente)
      <option value="{{ $cliente->id }}" data-carros="{{ $cliente->carros }}" data-offer="{{ $cliente->four_promotion }}">{{ $cliente->data }}</option>      
    @endforeach
  </select>
</div>

<div class="form-group col-sm-6 col-sm-offset-3">
  {!! Form::label('cliente', 'Carros:') !!}
  <select class="form-control col-sm-6 select2" id="facturas-carros" name="carro_id" tabindex="2" required>
  </select>
</div>


<div class="form-group col-sm-6 col-sm-offset-3">
  {!! Form::label('servicio', 'Servicio:') !!}
  <select class="form-control col-sm-6 select2" name="servicio" tabindex="3" required>
    <option></option>
    @foreach($servicios as $servicio)
      <option value="{{ $servicio->id }}">{{ $servicio->nombre . ' - Precio: ' . $servicio->precio }}</option>
    @endforeach
  </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-6 col-sm-offset-3">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('facturas.index') !!}" class="btn btn-default">Cancelar</a>
</div>