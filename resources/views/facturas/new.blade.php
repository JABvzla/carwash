@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Facturas
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    <form action="/facturas/new" method="post">
                        {!! csrf_field() !!}                        
                        <legend>Cliente</legend>
                        @include('clientes.fields')

                        <legend>Carro</legend>
                        @include('carros.fields')

                        <legend>Servicio</legend>
                        <div class="form-group col-sm-6 col-sm-offset-3">
                          {!! Form::label('servicio', 'Servicio:') !!}
                          <select class="form-control col-sm-6 select2" tabindex="3" name="servicio">
                            <option></option>
                            @foreach($servicios as $servicio)
                              <option value="{{ $servicio->id }}">{{ $servicio->nombre . ' - Precio: ' . $servicio->precio }}</option>
                            @endforeach
                          </select>
                        </div>


                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                            <a href="{!! route('facturas.create') !!}" class="btn btn-default">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
