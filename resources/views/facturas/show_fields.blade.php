<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $facturas->id !!}</p>
</div>

<!-- Servicio Field -->
<div class="form-group">
    {!! Form::label('servicio', 'Servicio:') !!}
    <p>{!! $facturas->servicio !!}</p>
</div>

<!-- Carro Field -->
<div class="form-group">
    {!! Form::label('carro', 'Carro:') !!}
    <p>{!! $facturas->carro !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $facturas->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $facturas->updated_at !!}</p>
</div>

