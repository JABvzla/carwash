<table class="table table-responsive" id="carros-table">
    <thead>
        <th>Modelo</th>
        <th>Placa</th>
        <th>Cliente</th>
        <th>Facturas</th>
        <th colspan="3">Acciones</th>
    </thead>
    <tbody>
    @foreach($carros as $carro)
        <tr>
            <td>{!! $carro->modelo !!}</td>
            <td>{!! $carro->placa !!}</td>
            <td>{!! $carro->cliente->data !!}</td>
            <td>{{ count($carro->facturas) }}</td>
            <td>
                {!! Form::open(['route' => ['carros.destroy', $carro->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('carros.show', [$carro->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('carros.edit', [$carro->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Está seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>