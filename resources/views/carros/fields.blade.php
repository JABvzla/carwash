<!-- Modelo Field -->
<div class="form-group col-sm-5 col-sm-offset-3">
    {!! Form::label('modelo', 'Modelo:') !!}
    {!! Form::text('modelo', null, ['class' => 'form-control']) !!}
</div>

<!-- Placa Field -->
<div class="form-group col-sm-5 col-sm-offset-3">
    {!! Form::label('placa', 'Placa:') !!}
    {!! Form::text('placa', null, ['class' => 'form-control']) !!}
</div>

@if( !isset($cedula) )
    <!-- Cliente Field -->
    <div class="form-group col-sm-5 col-sm-offset-3">
        {!! Form::label('cliente', 'Cliente:') !!}
        <select class="form-control select2" name="cliente_id">
          @foreach($clientes as $cliente)
          <option value="{{ $cliente->id }}" {{ ($cliente->id == $carros->clente_id)? 'selected=""' : '' }}>{{ $cliente->data }}</option>
          @endforeach
      </select>
    </div>
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('carros.index') !!}" class="btn btn-default">Cancelar</a>
    </div>
@endif
