<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();
Route::middleware(['auth'])->group(function () {

	Route::get('/home', 'HomeController@index')->name('home');


	Route::resource('clientes', 'clientesController');

	Route::resource('carros', 'carrosController');

	Route::resource('servicios', 'ServicioController');

	Route::get('facturas/new/{id}', 'FacturasController@new_fac');
	Route::post('facturas/new', 'FacturasController@new_storage');
	Route::resource('facturas', 'FacturasController');

});