<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatecarrosRequest;
use App\Http\Requests\UpdatecarrosRequest;
use App\Repositories\carrosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
// Models
use App\Models\carros;
use App\Models\clientes;

class carrosController extends AppBaseController
{
    /** @var  carrosRepository */
    private $carrosRepository;

    public function __construct(carrosRepository $carrosRepo)
    {
        $this->carrosRepository = $carrosRepo;
    }

    /**
     * Display a listing of the carros.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->carrosRepository->pushCriteria(new RequestCriteria($request));
        $carros = $this->carrosRepository->all();

        return view('carros.index')
            ->with('carros', $carros);
    }

    /**
     * Show the form for creating a new carros.
     *
     * @return Response
     */
    public function create()
    {
        $clientes = clientes::all();
        $carros = new carros();
        return view('carros.create')
                ->with('carros',$carros)
                ->with('clientes',$clientes);
    }

    /**
     * Store a newly created carros in storage.
     *
     * @param CreatecarrosRequest $request
     *
     * @return Response
     */
    public function store(CreatecarrosRequest $request)
    {
        $input = $request->all();

        $carros = $this->carrosRepository->create($input);

        Flash::success('Carro registrado correctamente.');

        return redirect(route('carros.index'));
    }

    /**
     * Display the specified carros.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $carros = $this->carrosRepository->findWithoutFail($id);

        if (empty($carros)) {
            Flash::error('Carros not found');

            return redirect(route('carros.index'));
        }

        return view('carros.show')->with('carros', $carros);
    }

    /**
     * Show the form for editing the specified carros.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $carros = $this->carrosRepository->findWithoutFail($id);

        if (empty($carros)) {
            Flash::error('Carros not found');

            return redirect(route('carros.index'));
        }

        $clientes = clientes::all();
        return view('carros.edit')
                ->with('carros', $carros)
                ->with('clientes',$clientes);
    }

    /**
     * Update the specified carros in storage.
     *
     * @param  int              $id
     * @param UpdatecarrosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecarrosRequest $request)
    {
        $carros = $this->carrosRepository->findWithoutFail($id);

        if (empty($carros)) {
            Flash::error('Carros not found');

            return redirect(route('carros.index'));
        }

        $carros = $this->carrosRepository->update($request->all(), $id);

        Flash::success('Carro actualizado correctamente.');

        return redirect(route('carros.index'));
    }

    /**
     * Remove the specified carros from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $carros = $this->carrosRepository->findWithoutFail($id);

        if (empty($carros)) {
            Flash::error('Carros not found');

            return redirect(route('carros.index'));
        }

        $this->carrosRepository->delete($id);

        Flash::success('Carro borrado correctamente.');

        return redirect(route('carros.index'));
    }
}
