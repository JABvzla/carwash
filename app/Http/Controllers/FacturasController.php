<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFacturasRequest;
use App\Http\Requests\UpdateFacturasRequest;
use App\Repositories\FacturasRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
// Models
use App\Models\Facturas;
use App\Models\clientes;
use App\Models\carros;
use App\Models\Servicio;


class FacturasController extends AppBaseController
{
    /** @var  FacturasRepository */
    private $facturasRepository;

    public function __construct(FacturasRepository $facturasRepo)
    {
        $this->facturasRepository = $facturasRepo;
    }

    /**
     * Display a listing of the Facturas.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->facturasRepository->pushCriteria(new RequestCriteria($request));
        $facturas = $this->facturasRepository->all();

        return view('facturas.index')
            ->with('facturas', $facturas);
    }

    /**
     * Show the form for creating a new Facturas.
     *
     * @return Response
     */
    public function create()
    {
        $clientes = clientes::with('carros')->orderBy('cedula','asc')->get();
        $servicios = Servicio::orderBy('nombre','asc')->get();
        return view('facturas.create')
                ->with('clientes',$clientes)
                ->with('servicios',$servicios);
    }

    /**
     * Store a newly created Facturas in storage.
     *
     * @param CreateFacturasRequest $request
     *
     * @return Response
     */
    public function store(CreateFacturasRequest $request)
    {
        $carro = carros::find( $request->carro_id );

        $cliente = $carro->cliente;

        $isPromo = $cliente->getFourPromotionAttribute();
        
        $this->save( $request->servicio, $carro->id, $isPromo );

        Flash::success('Factura registrada correctamente.');

        return redirect(route('facturas.index'));
    }

    /**
     * Display the specified Facturas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $facturas = $this->facturasRepository->findWithoutFail($id);

        if (empty($facturas)) {
            Flash::error('Facturas not found');

            return redirect(route('facturas.index'));
        }

        return view('facturas.show')->with('facturas', $facturas);
    }

    /**
     * Show the form for editing the specified Facturas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $facturas = $this->facturasRepository->findWithoutFail($id);

        if (empty($facturas)) {
            Flash::error('Facturas not found');

            return redirect(route('facturas.index'));
        }

        return view('facturas.edit')->with('facturas', $facturas);
    }

    /**
     * Update the specified Facturas in storage.
     *
     * @param  int              $id
     * @param UpdateFacturasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFacturasRequest $request)
    {
        $facturas = $this->facturasRepository->findWithoutFail($id);

        if (empty($facturas)) {
            Flash::error('Facturas not found');

            return redirect(route('facturas.index'));
        }

        $facturas = $this->facturasRepository->update($request->all(), $id);

        Flash::success('Facturas updated successfully.');

        return redirect(route('facturas.index'));
    }

    /**
     * Remove the specified Facturas from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $facturas = $this->facturasRepository->findWithoutFail($id);

        if (empty($facturas)) {
            Flash::error('Facturas not found');

            return redirect(route('facturas.index'));
        }

        $this->facturasRepository->delete($id);

        Flash::success('Factura borrada correctamente.');

        return redirect(route('facturas.index'));
    }

    public function new_fac($id)
    {
        $servicios = Servicio::all();
        return view('facturas.new')
                ->with('servicios',$servicios)
                ->with('cedula',$id);
    }

    public function new_storage(Request $request)
    {
        $cliente =  new clientes();
        $cliente->fill( $request->all() )->save();

        $carro =  new carros();
        $carro->fill( $request->all() );
        $carro->cliente_id = $cliente->id;
        $carro->save();

        $this->save( $request->servicio, $carro->id, false );

        return redirect('facturas');
    }

    private function save($service_id, $carro_id, $isPromo = false)
    {
        $servicio = Servicio::find( $service_id );
        $factura =  new Facturas();
        $factura->carro_id = $carro_id;
        $factura->servicio = $servicio->nombre;
        
        $precio = $servicio->precio;
        if( $isPromo ){
            $precio /=2;
        }

        $factura->precio = $precio;
        $factura->save();
    }
}