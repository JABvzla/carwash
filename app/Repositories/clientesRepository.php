<?php

namespace App\Repositories;

use App\Models\clientes;
use InfyOm\Generator\Common\BaseRepository;

class clientesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'apellido',
        'cedula'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return clientes::class;
    }
}
