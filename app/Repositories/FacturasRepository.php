<?php

namespace App\Repositories;

use App\Models\Facturas;
use InfyOm\Generator\Common\BaseRepository;

class FacturasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'servicio',
        'carro'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Facturas::class;
    }
}
