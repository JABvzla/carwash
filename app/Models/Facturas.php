<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Facturas
 * @package App\Models
 * @version May 21, 2017, 10:01 pm UTC
 */
class Facturas extends Model
{
    use SoftDeletes;

    public $table = 'facturas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'servicio',
        'precio',
        'carro_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'servicio' => 'string',
        'carro' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function carro()
    {
        return $this->belongsTo('App\Models\carros','carro_id');
    }    

    public function getCreatedAttribute()
    {
        return \Carbon\Carbon::parse($this->created_at)->format('d/m/Y');
    }
}
