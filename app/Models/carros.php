<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class carros
 * @package App\Models
 * @version May 21, 2017, 9:48 pm UTC
 */
class carros extends Model
{
    use SoftDeletes;

    public $table = 'carros';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'modelo',
        'placa',
        'cliente_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'modelo' => 'string',
        'placa' => 'string',
        'cliente_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'modelo' => 'required',
        'cliente_id' => 'required'
    ];
    
    public static function boot()
    {
        parent::boot();
    
        static::deleting(function($carro) { 
             $carro->facturas()->delete();
        });
    }

    public function cliente()
    {
        return $this->belongsTo('App\Models\clientes','cliente_id');
    }

    public function facturas()
    {
        return $this->hasMany('App\Models\Facturas','carro_id');
    }

    public function getDataAttribute()
    {
        return $this->modelo . ' - Placa: ' . $this->placa;
    }
    
}
