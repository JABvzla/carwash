<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class clientes
 * @package App\Models
 * @version May 21, 2017, 9:43 pm UTC
 */
class clientes extends Model
{
    use SoftDeletes;

    public $table = 'clientes';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre',
        'apellido',
        'cedula'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'apellido' => 'string',
        'cedula' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'apellido' => 'required',
        'cedula' => 'required'
    ];
    
    public static function boot()
    {
        parent::boot();
    
        static::deleting(function($cliente) { 
             $cliente->carros()->delete();
        });
    }

    public function carros()
    {
        return $this->hasMany('App\Models\carros','cliente_id');
    }
    
    public function facturas()
    {

    }

    public function getDataAttribute()
    {
        return $this->cedula . '  -  ' . $this->nombre . ' ' . $this->apellido;
    }

    public function getTotalFacturasAttribute()
    {
        $total = 0;
        foreach ($this->carros as $carro) {
            $total += count($carro->facturas); 
        }
        return $total;
    }

    public function getFourPromotionAttribute()
    {
        $total_facturas = $this->getTotalFacturasAttribute();
        $total_facturas++;
        return ( fmod($total_facturas, 4) == 0 ) && $total_facturas >= 4;
    }
}
